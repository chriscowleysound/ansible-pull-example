# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version = "3.4.3"
  hashes = [
    "h1:xZGZf18JjMS06pFa4NErzANI98qi59SEcBsOcS2P2yQ=",
    "zh:41c53ba47085d8261590990f8633c8906696fa0a3c4b384ff6a7ecbf84339752",
    "zh:59d98081c4475f2ad77d881c4412c5129c56214892f490adf11c7e7a5a47de9b",
    "zh:686ad1ee40b812b9e016317e7f34c0d63ef837e084dea4a1f578f64a6314ad53",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:84103eae7251384c0d995f5a257c72b0096605048f757b749b7b62107a5dccb3",
    "zh:8ee974b110adb78c7cd18aae82b2729e5124d8f115d484215fd5199451053de5",
    "zh:9dd4561e3c847e45de603f17fa0c01ae14cae8c4b7b4e6423c9ef3904b308dda",
    "zh:bb07bb3c2c0296beba0beec629ebc6474c70732387477a65966483b5efabdbc6",
    "zh:e891339e96c9e5a888727b45b2e1bb3fcbdfe0fd7c5b4396e4695459b38c8cb1",
    "zh:ea4739860c24dfeaac6c100b2a2e357106a89d18751f7693f3c31ecf6a996f8d",
    "zh:f0c76ac303fd0ab59146c39bc121c5d7d86f878e9a69294e29444d4c653786f8",
    "zh:f143a9a5af42b38fed328a161279906759ff39ac428ebcfe55606e05e1518b93",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.47.0"
  constraints = "~> 1.47.0"
  hashes = [
    "h1:6GKN5WfUZVVJ9cBHwN74RVOzMK2xlZx1qwGo8CUtPPk=",
    "zh:110dfeb02d47af0d1c71d41deac7bb0b23d0d5f9f2ac95f81fee7be1b7131852",
    "zh:164c141ca8d1d1b43b866150797f5e15855d48aaddd50a80ca320e638cdbbd3a",
    "zh:1be4fab5de93f2947c35df6676c67bf2ea410ec71f29e4a57661119c2f262d6a",
    "zh:40dc8f1ffc3521786b38427fe2c0f2ec0b102fed4284c08b2d092ebd1617a603",
    "zh:6335190faaeba5bd5a859df7075c6a820a1f492d1f2258296a4fb3170b4f0643",
    "zh:6460c8c651ba96d434e0db0e09cb02b6188861a8a683b1f9a488d0e119deeb71",
    "zh:6ba87e36384c8c165e0853366252451a20a2c1aeec1a2280eb208db200b5ce33",
    "zh:6ebbb699c8673ebbaa5fc880c7b3a8dcf69d51bf242c72b986cf6b919bac4969",
    "zh:719dc962699d17d03f017d27809c68162a2a849e478e04b858cd798be737a26a",
    "zh:7d865928a695dc3558f671da12dbb134d822968d2f0cb4b600ce79c41a0d1d4b",
    "zh:b5c50a6ab05cb18780f4d4b4d48f502d60f4121810127105f5f7d34d2dda9569",
    "zh:df3adcaacfdd3696a9df768beb3ec997410724c5dbd1cc5fd023e6644686bd74",
    "zh:f836c285e5e36ddaa3d0d13f4e89fcb9047356cfb09c77651cdf97b6394af7fa",
    "zh:f91081a4f26b69b8325149353d3199838466e971853a2d1e9c2917051a6fdd6d",
  ]
}

provider "terraform.rome.ubisoft.org/itps/uhs" {
  version     = "0.0.2"
  constraints = "0.0.2"
  hashes = [
    "h1:ONHegIKlD4cKBuI2K8JuIUfQugofsROVi81Wy8d/oqs=",
    "zh:94e45746e49e185fc728a7fbe4eb73c91957b236a79a6570cc1862b7e282c148",
  ]
}
