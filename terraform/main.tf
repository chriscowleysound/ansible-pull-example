data "template_file" "ansible-pull-userdata" {
  template = file("./templates/ansible-pull-userdata.yaml")
}

data "template_cloudinit_config" "ansible-pull" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "ansible-pull.cfg"
    content_type = "text/cloud-config"

    content = data.template_file.ansible-pull-userdata.rendered
  }
}
module "pull" {
  source = "git::ssh://git@gitlab-ncsa.ubisoft.org/cloud-product-specialist/terraform-modules/ubicloud-network.git//modules/multi-az/"
  #source = "../../terraform-modules/ubicloud-network/modules/multi-az/"
  region = "emea-west-pdc-z01"
  network = {
    name        = "lifecycle-pull-net"
    subnet_name = "lifecycle-pull-subnet"
  }
}

module "pull-instance" {
  source       = "git::ssh://git@gitlab-ncsa.ubisoft.org/cloud-product-specialist/terraform-modules/ubicloud-instance.git//modules/multi-az/"
  region       = "emea-west-pdc-z01"
  count        = 1
  hostname     = "pull-${count.index}"
  image        = "rocky-8"
  network_base = module.pull.base_name
  flavor_name  = "ug4n.small"
  key_pair     = "ccowley"
  security_groups = [
    "default",
    "allow_ssh_access",
  ]
  user_data = data.template_cloudinit_config.ansible-pull.rendered
  depends_on = [
    module.pull
  ]
  metadata = {
    role = "pull"
  }
}

output "pull-ip" {
  value = module.pull-instance[0].access_ip
}

